# -*- shell-script -*-

run_createrepo() {
  ( set -x
    /bin/pwd
    createrepo_c -d -q -x '*.src.rpm' PACKAGES
  )
}

run_buildsrpm() {
  if [ -n "$ENTRYPOINT_DEBUG" ]; then
    set -x
  fi

  local specfile="$1"
  local name="$( specname="${specfile##*/}"; echo "${specname%.spec}" )"
  local sources="${specfile%/*}"
  local srpm_dir="BUILD/$name"

  ( set -x
    rm -rf "$srpm_dir"
    mkdir -p "$srpm_dir"
    mock --buildsrpm --old-chroot \
         --resultdir "$srpm_dir" \
         --sources "$sources" \
         --spec "$specfile" \
    ;
  )
}

run_buildrpm() {
  if [ -n "$ENTRYPOINT_DEBUG" ]; then
    set -x
  fi

  local specfile="$1"
  local name="$( specname="${specfile##*/}"; echo "${specname%.spec}" )"
  local srpm_dir="BUILD/$name"
  local srpm_file=$( 
    srpm_path="$( awk '/^Wrote:.+\.src\.rpm$/ { print $2 }' "$srpm_dir/build.log" | tail -1 )"
    echo "${srpm_path##*/}"
  )

  if [ -z "$srpm_file" ]; then
    echo "$prog: no src.rpm file found at \`$PWD/$srpm_dir/build.log'" >&2
    return 1
  fi

  ( set -x
    mock --rebuild --old-chroot \
         --resultdir "PACKAGES/$name" \
         "$srpm_dir/$srpm_file" \
    ;
  )
}
