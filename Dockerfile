ARG BASE_IMAGE
FROM $BASE_IMAGE

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

COPY entrypoint /usr/local/entrypoint

RUN set -xe \
&& curl -ksL http://bit.ly/netsetup-install-yumrepo | /bin/sh /dev/stdin ius \
&& yum update -y \
&& ln -s /usr/local/entrypoint/main /usr/local/bin/docker-entrypoint \
&& ln -s /usr/local/bin/docker-entrypoint /docker-entrypoint \
&& yum install -y rpmdevtools rpmbuild yum-utils diff patch mock createrepo_c rsync \
&& yum clean all \
&& sed -i '\!config_opts\['"'docker_unshare_warning'"'!{s!^#\s*!!; s!True!False!}' /etc/mock/site-defaults.cfg \
&& useradd mock -g mock \
&& mkdir -p /rpmbuild/{BUILD,PACKAGES} \
;

# make sure that /usr/bin/ is listed before /usr/sbin
ENV PATH /usr/bin:/usr/sbin:/sbin:/bin

VOLUME ["/rpmbuild"]

WORKDIR /rpmbuild
ENTRYPOINT ["/docker-entrypoint"]
USER mock
